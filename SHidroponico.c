#include <TroykaDHT.h>
#include <SoftwareSerial.h>
#include <Time.h>
#include <TimeLib.h>
//------------Thingspeak--------------------------------  
String myAPIkey = "PFT8TOCBZAFUIU96";
String canalID1 = "880401"; 
//------------------------------------------------------  
//--------pH------------------------------------------
const int analogPin = A0;
int pH;   //posicion del potenciometro
//-------------------------------------------------------
//--------DHT22------------------------------------------
DHT dht(4, DHT11);
float humedad, temperatura; 
//-------------------------------------------------------
//------------Serial-------------------------------------
SoftwareSerial ESP8266(2,3); // Rx,  Tx
//-------------------------------------------------------
//--------------Timer------------------------------------
time_t t;
int hora;
int minuto;
unsigned long retardo = 0;
int flag_hora = 1;
//-------------------------------------------------------
//-----------------------Sensores---------------------------
int tiempo_sensores_actual = 0;
int flag_wifi = 1;
//-------------------------------------------------------
//-----------------------Bomba---------------------------
const int pin_bomba = 9;
int estado_bomba;
int tiempo_bomba_actual = 0;
unsigned long tiempo_bomba_on = 1;//15 min
unsigned long tiempo_bomba_off = 1;//45 min
//-------------------------------------------------------
//-----------------------Ventilador----------------------
const int pin_ventilador = 8;
int estado_ventilador;
//-------------------------------------------------------
//-----------------------Leds---------------------------
const int pin_led = 12;
int estado_led;
int tiempo_led_actual = 0;
unsigned long tiempo_led_on = 2;//18h
unsigned long tiempo_led_off = 1;//6h
//-------------------------------------------------------
int estado = 0;

void setup(){
  Serial.begin(9600); 
  ESP8266.begin(9600);
  dht.begin();
  //seteo de pines
  pinMode(pin_bomba, OUTPUT);  //definir pin_bomba como salida
  pinMode(pin_led, OUTPUT);  //definir pin_led como salida
  pinMode(pin_ventilador, OUTPUT);  //definir pin_led como salida

  controlHardware();
}

void loop(){
  t = now();
  temporizador();
  
  switch (estado){
    case 0:
      estado = setupWifi();      
    break;
    case 1:
      estado = obtenerHora();     
    break;
    case 2:
      estado = intervalosensores();     
    break;
    case 3:
      estado = intervalomotorON();
    break; 
    case 4:
      estado = intervalomotorOFF();
    break;
    case 5:
      estado = intervaloLEDON();  
    break;
    case 6:
      estado = intervaloLEDOFF();
    break;
    case 8:
    break;
  }
}

void temporizador(){ 
  estado_led=digitalRead(pin_led);
  estado_bomba=digitalRead(pin_bomba);
  
  if((minute(t) == tiempo_sensores_actual || minute(t) == (tiempo_sensores_actual + 1)) & (second(t) == 10)){
    estado = 2;
  }  
  if( ((hour(t) >= 6) & (hour(t) <= 23)) & (minute(t) == (tiempo_bomba_actual)||minute(t) == (tiempo_bomba_actual + tiempo_bomba_off)) & (second(t) == 1) & (estado_bomba == 0) ){  // quitar ||minute(t) == (tiempo_bomba_actual + 2) para q sea cada hora
    estado = 3;
  }   
  if((minute(t) == (tiempo_bomba_on + tiempo_bomba_actual)) & (second(t) == 1) & (estado_bomba == 1)){      
    estado = 4;
  }     
  
  if( ((hour(t) >= 6) & (hour(t) <= 23)) & (minute(t) == (tiempo_led_actual)||minute(t) == (tiempo_led_actual + tiempo_led_off)) & (second(t) == 3) & (estado_led == 0)){  
    estado = 5;
  }   
  if((minute(t) == (tiempo_led_on + tiempo_led_actual)) & (second(t) == 3)& (estado_led == 1)){      
    estado = 6;
  } 
}

int intervalosensores(){
  int n;  
  tiempo_sensores_actual = minute(t);
  
  ESP8266.println("AT+CWJAP?\r\n");
  if(ESP8266.find("OK") != true){
    Serial.println("WIFI desconectado");
    flag_wifi = 1;
  }
  
  if(flag_wifi == 1){
    n = setupWifiReintento();
  }
  
  readSensors();  
  if(millis() > retardo + (1000) & flag_wifi == 0){
    Serial.print("Leyendo sensores a las: ");
    Serial.print(hour(t));
    Serial.print(":");
    Serial.print(minute(t));
    Serial.print(":");
    Serial.print(second(t));
    Serial.print(" hs \r\n\r\n");    
    writeThingSpeak();
    retardo = millis();
    n = 8;
  }             
  if (flag_hora == 1 && flag_wifi == 0){
    Serial.println("Sincronizando hora, fallo la conexion al inicio\r\n");
    n = obtenerHoraReintento();
  }
  if(timeStatus() != timeSet){
    Serial.println("Sincronizando hora\r\n");
    n = obtenerHoraReintento();
  }   
  tiempo_sensores_actual = minute(t);
  
  return n;
}

void controlHardware(){
  Serial.println("Controlando Hardware\r\n");
  digitalWrite(pin_bomba,HIGH);
  digitalWrite(pin_led,HIGH);
  digitalWrite(pin_ventilador,HIGH);
  delay(2000);
  digitalWrite(pin_led,LOW);
  delay(500);
  digitalWrite(pin_led,HIGH);
  delay(2000);
  digitalWrite(pin_led,LOW);
  delay(500);
  digitalWrite(pin_led,HIGH);
  delay(2000);
  digitalWrite(pin_bomba,LOW);
  digitalWrite(pin_led,LOW);
  digitalWrite(pin_ventilador,LOW);
 
  dht.read();
  switch(dht.getState()){
    case DHT_OK:
      Serial.print("Temperatura = ");
      Serial.print(dht.getTemperatureC());
      Serial.println(" C \t");
      Serial.print("Humedad = ");
      Serial.print(dht.getHumidity());
      Serial.println(" %");
      Serial.println("Controlando Hardware: OK\r\n");
      break;
    case DHT_ERROR_CHECKSUM:
      Serial.println("Error Checksum");
      break;
    case DHT_ERROR_TIMEOUT:
      Serial.println("Error de tiempo de espera");
      break;
    case DHT_ERROR_NO_REPLY:
      Serial.println("Sensor no conectado");
      break;
  }
}

int setupWifi(){
  int n;
  int i = 0;  
  REINTENTAR:;
  i ++;
  if(flag_wifi == 1){
  ESP8266.println("AT+RST\r\n");
  delay(2000);
  Serial.println("Conectando WiFi");
  ESP8266.println("AT+CWJAP=\"El mono de jade\",\"321pepe321\"\r\n");
  Serial.println("...");
  delay(5000);
  ESP8266.println("AT+CWJAP=\"El mono de jade\",\"321pepe321\"\r\n");
  delay(5000);
  ESP8266.println("AT+CIPMUX=1\r\n");
  delay(3000);
  ESP8266.println("AT+CIPSERVER=1,80\r\n");
  }
  ESP8266.println("AT+CWJAP?\r\n");
  if(ESP8266.find("OK")){
    Serial.println("WIFI conectado\r\n");
    n = 1;
    flag_wifi = 0;
  }
  if(flag_wifi == 1){
    Serial.println("Error al conectarse en la red, reintento:");
    Serial.println(i);
    n = 8;
    while( i < 3 ){    
      goto REINTENTAR;
    }
    
    hora = 6;
    minuto = 0;
    Serial.println("Error al leer hora. Establecer hora por defecto:\r\n");
    Serial.print("Hora=");
    Serial.println(hora);
    Serial.print("Min=");
    Serial.println(minuto);
    flag_hora = 1;
  
    setTime(hora, minuto, 0, 1, 1, 2020);
    t = now();//cargo hora actual
  
  
    tiempo_led_actual = minute(t);
   
    tiempo_bomba_actual = minute(t);
    tiempo_sensores_actual = minute(t); 
    n = 8;
  }
  
  return n;
}

int setupWifiReintento(){
  int n;
  int i = 0;
  REINTENTAR1:;
  i ++;
  if(flag_wifi == 1){
  ESP8266.println("AT+RST\r\n");
  delay(2000);
  Serial.println("Conectando WiFi");
  ESP8266.println("AT+CWJAP=\"El mono de jade\",\"******\"\r\n");
  Serial.println("...");
  delay(5000);
  ESP8266.println("AT+CWJAP=\"El mono de jade\",\"*******\"\r\n");
  delay(5000);
  ESP8266.println("AT+CIPMUX=1\r\n");
  delay(3000);
  ESP8266.println("AT+CIPSERVER=1,80\r\n");
  }
  ESP8266.println("AT+CWJAP?\r\n");
  if(ESP8266.find("OK")){
    Serial.println("WIFI conectado");
    flag_wifi = 0;
  }
  if(flag_wifi == 1){
    Serial.print("Error al conectarse en la red, reintento:");
    Serial.println(i);
    while(i < 2){
      goto REINTENTAR1;
    }
    flag_wifi = 1;
  }  
  
  n = 8; 
  
  return n;
}

int obtenerHora(){
  int n;
  if(flag_hora == 1){
    int command = readThingSpeak1(canalID1); 
    hora = command; 
    Serial.print("Hora=");
    Serial.println(hora);
    delay (5000); 
    command = readThingSpeak2(canalID1); 
    minuto = command;
    Serial.print("Min=");
    Serial.println(minuto); 
    delay (5000);
    flag_hora = 0;
  }
  if((hora < 0) || (hora > 24) || (minuto < 0) || (minuto > 60)){
    hora = 6;
    minuto = 0;
    Serial.print("Error al leer hora. Establecer hora por defecto:\r\n");
    Serial.print("Hora=");
    Serial.println(hora);
    Serial.print("Min=");
    Serial.println(minuto);
    flag_hora = 1;
  }
  setTime(hora, minuto, 0, 1, 1, 2020);
  t = now();//cargo hora actual
  
  if((hour(t) >= 6) & (hour(t) <= 23)){
    tiempo_led_actual = minute(t);
  }  
  tiempo_bomba_actual = minute(t);
  tiempo_sensores_actual = minute(t); 
  n = 8;
  
  return n; 
}

int obtenerHoraReintento(){
  int n,h,m; 
  if(flag_hora == 1){
    int command = readThingSpeak1(canalID1); 
    h = command; 
    Serial.print("Hora=");
    Serial.println(h);
    delay (5000); 
    command = readThingSpeak2(canalID1); 
    m = command;
    Serial.print("Min=");
    Serial.println(m); 
    delay (5000);
    flag_hora = 0;
    
    if((h < 0) || (h > 24) || (m < 0) || (m > 60)){
      Serial.print("Error al leer hora. Sigue la hora por defecto:\r\n");
      Serial.print("Hora=");
      Serial.println(hour(t));
      Serial.print("Min=");
      Serial.println(minute(t));
      flag_hora = 1;
    } 
    else{
      hora = h;
      minuto = m;
      setTime(hora, minuto, 11, 1, 1, 2020);
      t = now();//cargo hora actual
      if((hour(t) >= 6) & (hour(t) <= 23)){
        tiempo_led_actual = minute(t);
      }
      tiempo_bomba_actual = minute(t);
      tiempo_sensores_actual = minute(t);
    }     
  } 
  n = 8;
  
  return n; 
}
void readSensors(void){
  dht.read();
  temperatura = dht.getTemperatureC();
  humedad = dht.getHumidity();
  if(temperatura > 28 & estado_ventilador == 0){
    digitalWrite(pin_ventilador,true);
    Serial.println("Cooler encendido a las: ");
    Serial.print(hour(t));
    Serial.print(":");          
    Serial.print(minute(t));
    Serial.print(":");
    Serial.print(second(t));
    Serial.print(" hs \r\n\r\n"); 
    estado_ventilador = 1;
  }
  else if(temperatura < 27 & estado_ventilador == 1){
    digitalWrite(pin_ventilador,false);
    Serial.println("Cooler apagado a las: ");
    Serial.print(hour(t));
    Serial.print(":");          
    Serial.print(minute(t));
    Serial.print(":");
    Serial.print(second(t));
    Serial.print(" hs \r\n\r\n");
    estado_ventilador = 0;
  }
  pH = map(analogRead(analogPin), 0, 675, 0, 14);// convertir a porcentaje 5[V]/2^10=4.88m  =>   3.3/4.88m = 676 
  estado_led=digitalRead(pin_led);
  estado_bomba=digitalRead(pin_bomba);
}

int intervaloLEDON(){
  int n;
  if(millis() > retardo + (1000)){
    tiempo_led_actual = minute(t);
    digitalWrite(pin_led,HIGH);
    Serial.println("Led encendido a las: ");
    Serial.print(hour(t));
    Serial.print(":");          
    Serial.print(minute(t));
    Serial.print(":");
    Serial.print(second(t));
    Serial.print(" hs \r\n\r\n");
    retardo = millis();
    n = 8;  
  }
  
  return n;  
}

int intervaloLEDOFF(){
  int n;
  if(millis() > retardo + (1000)){
    tiempo_led_actual = minute(t);
    digitalWrite(pin_led,LOW);
    Serial.println("Led apagado a las: ");
    Serial.print(hour(t));
    Serial.print(":");          
    Serial.print(minute(t));
    Serial.print(":");
    Serial.print(second(t));
    Serial.print(" hs \r\n\r\n");
    retardo = millis();
    n = 8;
  }
  
  return n;        
}

int intervalomotorON(){
  int n;
  if(millis() > retardo + (1000)){
    tiempo_bomba_actual = minute(t);
    digitalWrite(pin_bomba,HIGH);
    Serial.println("Bomba encendida a las: ");
    Serial.print(hour(t));
    Serial.print(":");          
    Serial.print(minute(t));
    Serial.print(":");
    Serial.print(second(t));
    Serial.print(" hs \r\n\r\n");
    retardo = millis();
    n = 8;
  } 
  
  return n;
}

int intervalomotorOFF(){
  int n;
  if(millis() > retardo + (1000)){
    tiempo_bomba_actual = minute(t);
    digitalWrite(pin_bomba,LOW);
    Serial.println("Bomba apagada a las: ");
    Serial.print(hour(t));
    Serial.print(":");          
    Serial.print(minute(t));
    Serial.print(":");
    Serial.print(second(t));
    Serial.print(" hs \r\n\r\n");
    retardo = millis();
    n = 8;
  }
  
  return n;
}

/********* Read Actuators command from ThingSpeak *************/
int readThingSpeak1(String channelID){
  startThingSpeakCmd();
  int command;
  int a,b;
  // preparacao da string GET
  String getStr = "GET /channels/";
  getStr += channelID;
  getStr +="/fields/7/last";
  getStr += "\r\n";

  String messageDown = GetThingspeakcmd1(getStr);
  if (messageDown[5] == 49){
    command = messageDown[7]-48; 
    Serial.print("Command received: ");
    Serial.println(command);
  }
  else{
    a = messageDown[7]-48;
    b = messageDown[8]-48;
    command = (a*10)+b;
  }
  
  return command;
}
/********* Read Actuators command from ThingSpeak *************/
int readThingSpeak2(String channelID){
  startThingSpeakCmd();
  int command;
  int a,b;
  // preparacao da string GET
  String getStr = "GET /channels/";
  getStr += channelID;
  getStr +="/fields/5/last";
  getStr += "\r\n";

  String messageDown = GetThingspeakcmd1(getStr);
  if (messageDown[5] == 49){
    command = messageDown[7]-48; 
    Serial.print("Command received: ");
    Serial.println(command);
  }
  else{
    a = messageDown[7]-48;
    b = messageDown[8]-48;
    command = (a*10)+b;
  }
  
  return command;
}

void writeThingSpeak(void){
  startThingSpeakCmd();
  // preparacao da string GET
  String getStr = "GET /update?api_key=";
  getStr += myAPIkey;
  getStr +="&field1=";
  getStr += String(temperatura);
  getStr +="&field2=";
  getStr += String(humedad);
  getStr +="&field3=";
  getStr += String(pH);
  getStr +="&field4=";
  getStr += String(estado_led);
  getStr +="&field6=";
  getStr += String(estado_bomba);
  getStr +="&field8=";
  getStr += String(estado_ventilador);
  getStr += "\r\n\r\n";
  GetThingspeakcmd(getStr); 
}

void startThingSpeakCmd(void){
  ESP8266.flush();//limpa o buffer antes de começar a gravar
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += "184.106.153.149"; // api.thingspeak.com IP address
  cmd += "\",80"; //puerto 80
  ESP8266.println(cmd);
  Serial.print("Comandos de inicio: ");
  Serial.println(cmd);
  while (ESP8266.find("Error")){
    Serial.println("AT+CIPSTART error");
    return;
  }
}

String GetThingspeakcmd(String getStr){
  String cmd = "AT+CIPSEND=";
  cmd += String(getStr.length());
  ESP8266.println(cmd);
  Serial.println(cmd);

 // if(ESP8266.find(">")){
    ESP8266.print(getStr);
    Serial.println(getStr);
    delay(500);
    String messageBody = "";
    while (ESP8266.available()){
      String line = ESP8266.readStringUntil('\n');
      if (line.length() == 1){ 
        messageBody = ESP8266.readStringUntil('\n');
      }
    }
    Serial.println(messageBody);
    
    return messageBody;  
}

String GetThingspeakcmd1(String getStr){
  String cmd = "AT+CIPSEND=";
  cmd += String(getStr.length());
  ESP8266.println(cmd);
  Serial.println(cmd);

  if(ESP8266.find(">")){
    ESP8266.print(getStr);
    Serial.println(getStr);
    delay(500);
    String messageBody = "";
    while (ESP8266.available()){
      String line = ESP8266.readStringUntil('\n');
      if (line.length() == 1){ 
        messageBody = ESP8266.readStringUntil('\n');
      }
    }
    Serial.println(messageBody);
    
    return messageBody;
  }
  else{
    ESP8266.println("AT+CIPCLOSE\r\n");     
    Serial.println("AT+CIPCLOSE");
    delay(2000);
    flag_wifi = 1;
  }  
}
