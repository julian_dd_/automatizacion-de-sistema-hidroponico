# Automatización de sistema hidropónico utilizando IoT

Autor: Delle Donne Julian

#### **Resumen**

En los últimos años, el avance de la informática y la tecnología permitió incorporar el control y ejecución de actividades, que han hecho de la automatización del cultivo hidropónico una realidad. Un cultivo hidropónico realizado en un área confinada y climatizada es un sistema altamente repetible, que en consecuencia, se ha constituido en una herramienta.
En este proyecto se desarrolla un prototipo de un sistema automatizado para cultivos hidropónicos utilizando tecnología IoT (Internet of Things), el cual permite gestionar los datos de nuestro sistema en cualquier momento, desde cualquier dispositivo con acceso a Internet y actuar automáticamente en consecuencia a estos.

#### **Objetivos y motivación**

El presente trabajo investiga sobre nuevas formas de cultivo y su desarrollo para una mejor calidad de vida. El mismo esta orientado a explicar las partes que componen el proyecto, sus funciónes, componentes, etc. y muy brevemente una noción respecto del área de la hidropónia. La idea de la autosustentabilidad es el objetivo principal de este proyecto, teniendo como meta que este se pueda repetir en hogares donde no se dispone de un terreno para poder cultivar.
El aumento de la población parece inherente, que trae consigo, un aumento de producción alimenticia. Distintas áreas de la ciencia proponen métodos diferentes para solucionarlo, como son las alteraciones genéticas y utilización de agro-químicos en los cultivos. Estos provocan la aparición de diferentes problemas tanto para la salud de las personas como para el medio ambiente.
Se considera a la hidropónia una posible solución a estos problemas sin dejar de lado el avance tecnológico y la facilidad que esta pueda brindar al usuario para su utilización.


En el siguiente enlace se puede ver una pequeña presentación del proyecto:
https://www.youtube.com/watch?v=RLNuwi7UMCg&t=1s


